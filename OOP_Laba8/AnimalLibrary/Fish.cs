﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Laba8.AnimalLibrary
{

    public class Fish : Animal
    {
        public bool IsOceanic { get; set; }
        public Fish() : base()
        {
            IsOceanic = false;
        }

        public Fish(int age, double weight, string gender, bool isOceanic)
            : base(age, weight, gender)
        {
            IsOceanic = isOceanic;
        }
        public Fish(Fish other) : base(other)
        {
            IsOceanic = other.IsOceanic;
        }
        public void ChangeType(bool isOceanic)
        {
            IsOceanic = isOceanic;
        }
        public override void ShowInfo()
        {
            base.ShowInfo();  
            Console.WriteLine($"Риба: океанічна - {IsOceanic}");
        }
        public override bool Equals(object obj)
        {
            if (!base.Equals(obj))
                return false;

            Fish other = (Fish)obj;
            return IsOceanic == other.IsOceanic;
        }

      
        public override int GetHashCode()
        {
            return base.GetHashCode() ^ (IsOceanic.GetHashCode() * 5);
        }
    }   
}
