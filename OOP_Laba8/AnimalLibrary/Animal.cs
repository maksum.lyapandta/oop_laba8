﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Laba8.AnimalLibrary
{

    public class Animal
    {
        public int Age { get; set; }
        public double Weight { get; set; }
        public string Gender { get; set; }

        public Animal()
        {
            Age = 0;
            Weight = 0.0;
            Gender = "Невідомий";
        }

        public Animal(int age, double weight, string gender)
        {
            Age = age;
            Weight = weight;
            Gender = gender;
        }
        public Animal(Animal other)
        {
            Age = other.Age;
            Weight = other.Weight;
            Gender = other.Gender;
        }
        public void ChangeAge(int newAge)
        {
            Age = newAge;
        }

        public void ChangeWeight(double newWeight)
        {
            Weight = newWeight;
        }
        public virtual void ShowInfo()
        {
            Console.WriteLine($"Тварина: Вік - {Age}, Вага - {Weight}, Стать - {Gender}");
        }
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Animal other = (Animal)obj;
            return Age == other.Age && Weight == other.Weight && Gender == other.Gender;
        }

       
        public override int GetHashCode()
        {
            return (Age.GetHashCode() * 31) ^ (Weight.GetHashCode() * 17) ^ (Gender.GetHashCode() * 7);
        }
    }

  
   

   

}
