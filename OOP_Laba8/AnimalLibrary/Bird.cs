﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Laba8.AnimalLibrary
{
    public class Bird : Animal
    {
        public bool CanFly { get; set; }

        public Bird(): base()
        {
            CanFly = false;
        }

        public Bird(int age, double weight, string gender, bool canFly)
            : base(age, weight, gender)
        {
            CanFly = canFly;
        }

        public Bird(Bird other) : base(other)
        {
            CanFly = other.CanFly;
        }
        public void ChangeFlightAbility(bool canFly)
        {
            CanFly = canFly;
        }
        public override void ShowInfo()
        {
            base.ShowInfo(); 
            Console.WriteLine($"Птах: Може літати- {CanFly}");
        }
        public override bool Equals(object obj)
        {
            if (!base.Equals(obj))
                return false;

            Bird other = (Bird)obj;
            return CanFly == other.CanFly;
        }

      
        public override int GetHashCode()
        {
            return base.GetHashCode() ^ (CanFly.GetHashCode() * 13);
        }
    }
}
