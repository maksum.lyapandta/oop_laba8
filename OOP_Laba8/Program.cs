﻿using OOP_Laba8;
using OOP_Laba8.AnimalLibrary;
using System;

class Program
{
    static void Main()
    {
        bool exit = false;

        do
        {
            Console.WriteLine("1. Створіть тварину та керуйте нею");
            Console.WriteLine("2.Створення риби та керування нею");
            Console.WriteLine("3. Створіть птаха та керуйте ним");
            Console.WriteLine("4. Вийти");

            Console.Write("Виберіть опцію (1-4): ");
            string choice = Console.ReadLine();

            switch (choice)
            {
                case "1":
                    PerformAnimalOperations();
                    break;
                case "2":
                    PerformFishOperations();
                    break;
                case "3":
                    PerformBirdOperations();
                    break;
                case "4":
                    exit = true;
                    break;
                default:
                    Console.WriteLine("Invalid choice. Please enter a number from 1 to 4.");
                    break;
            }

        } while (!exit);
    

Animal[] animals = new Animal[3];

animals[0] = new Animal(5, 10.5, "Чоловік");
        animals[1] = new Fish(2, 1.5, "Жінка", true);
        animals[2] = new Bird(3, 0.8, "Жінка", false);

        foreach (var animal in animals)
        {
            animal.ShowInfo();
            Console.WriteLine();
        }

      
        Fraction defaultFraction = new Fraction();
        Console.WriteLine("Дріб за замовчуванням: " + defaultFraction);

       
        Fraction fractionWithParams = new Fraction(3, 4);
        Console.WriteLine("Дріб з параметрами: " + fractionWithParams);

      
        Fraction copyFraction = new Fraction();
        Console.WriteLine("Копіювати дріб: " + copyFraction);

    
        Fraction fraction1 = new Fraction(1, 2);
        Fraction fraction2 = new Fraction(2, 3);

        Fraction sum = fraction1 + fraction2;
        Console.WriteLine($"{fraction1} + {fraction2} = {sum}");

        Fraction difference = fraction1 - fraction2;
        Console.WriteLine($"{fraction1} - {fraction2} = {difference}");

        Fraction product = fraction1 * fraction2;
        Console.WriteLine($"{fraction1} * {fraction2} = {product}");

        Fraction quotient = fraction1 / fraction2;
        Console.WriteLine($"{fraction1} / {fraction2} = {quotient}");

       
        Console.WriteLine($"{fraction1} > {fraction2}: {fraction1 > fraction2}");
        Console.WriteLine($"{fraction1} >= {fraction2}: {fraction1 >= fraction2}");
        Console.WriteLine($"{fraction1} < {fraction2}: {fraction1 < fraction2}");
        Console.WriteLine($"{fraction1} <= {fraction2}: {fraction1 <= fraction2}");
        Console.WriteLine($"{fraction1} == {fraction2}: {fraction1 == fraction2}");
        Console.WriteLine($"{fraction1} != {fraction2}: {fraction1 != fraction2}");

      
        double decimalValue = fraction1;
        Console.WriteLine($"{fraction1} як дабл: {decimalValue}");

        Console.ReadLine();
    }
    static void PerformAnimalOperations()
    {
        Console.WriteLine("\nСтворення та маніпулювання твариною:");

        Animal animal = new Animal(5, 10.5, "Male");
        DisplayAnimalInfo(animal);

        animal.ChangeAge(6);
        animal.ChangeWeight(11.2);
        DisplayAnimalInfo(animal);
    }

    static void PerformFishOperations()
    {
        Console.WriteLine("\nСтворення та маніпулювання рибою:");

        Fish fish = new Fish(2, 1.5, "Female", true);
        DisplayFishInfo(fish);

        fish.ChangeType(false);
        DisplayFishInfo(fish);
    }
    static void PerformBirdOperations()
    {
        Console.WriteLine("\nСтворення та маніпулювання птахом:");

        Bird bird = new Bird(3, 0.8, "Male", false);
        DisplayBirdInfo(bird);

        bird.ChangeFlightAbility(true);
        DisplayBirdInfo(bird);
    }
    static void DisplayAnimalInfo(Animal animal)
    {
        Console.WriteLine("Тварина: Вік - {0}, Вага - {1}, Стать - {2}", animal.Age, animal.Weight, animal.Gender);
    }
    static void DisplayFishInfo(Fish fish)
    {
        Console.WriteLine("Риба: вік - {0}, вага - {1}, стать - {2}, океанічна - {3}", fish.Age, fish.Weight, fish.Gender, fish.IsOceanic);
    }

    static void DisplayBirdInfo(Bird bird)
    {
        Console.WriteLine("Птах: Вік - {0}, Вага - {1}, Стать - {2}, Може літати - {3}", bird.Age, bird.Weight, bird.Gender, bird.CanFly);
    }


}
