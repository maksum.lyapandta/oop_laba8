﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OOP_Laba8
{
    using System;

    public class Fraction
    {
        private int numerator;
        private int denominator;

       
        public Fraction()
        {
            numerator = 0;
            denominator = 1;
        }

       
        public Fraction(int numerator, int denominator)
        {
            if (denominator == 0)
                throw new ArgumentException("Знаменник не може дорівнювати нулю.");

            this.numerator = numerator;
            this.denominator = denominator;

            Simplify();
        }

      
        public static implicit operator double(Fraction fraction)
        {
            return (double)fraction.numerator / fraction.denominator;
        }

       
        public static Fraction operator +(Fraction fraction)
        {
            return new Fraction(fraction.numerator, fraction.denominator);
        }

     
        public static Fraction operator -(Fraction fraction)
        {
            return new Fraction(-fraction.numerator, fraction.denominator);
        }

       
        public static Fraction operator +(Fraction fraction1, Fraction fraction2)
        {
            int newNumerator = fraction1.numerator * fraction2.denominator + fraction2.numerator * fraction1.denominator;
            int newDenominator = fraction1.denominator * fraction2.denominator;

            return new Fraction(newNumerator, newDenominator);
        }

       
        public static Fraction operator -(Fraction fraction1, Fraction fraction2)
        {
            int newNumerator = fraction1.numerator * fraction2.denominator - fraction2.numerator * fraction1.denominator;
            int newDenominator = fraction1.denominator * fraction2.denominator;

            return new Fraction(newNumerator, newDenominator);
        }

       
        public static Fraction operator *(Fraction fraction1, Fraction fraction2)
        {
            int newNumerator = fraction1.numerator * fraction2.numerator;
            int newDenominator = fraction1.denominator * fraction2.denominator;

            return new Fraction(newNumerator, newDenominator);
        }

      
        public static Fraction operator /(Fraction fraction1, Fraction fraction2)
        {
            if (fraction2.numerator == 0)
                throw new DivideByZeroException("Не можна ділити на нуль.");

            int newNumerator = fraction1.numerator * fraction2.denominator;
            int newDenominator = fraction1.denominator * fraction2.numerator;

            return new Fraction(newNumerator, newDenominator);
        }

       
        public static bool operator >(Fraction fraction1, Fraction fraction2)
        {
            return fraction1.CompareTo(fraction2) > 0;
        }

        public static bool operator >=(Fraction fraction1, Fraction fraction2)
        {
            return fraction1.CompareTo(fraction2) >= 0;
        }

        public static bool operator <(Fraction fraction1, Fraction fraction2)
        {
            return fraction1.CompareTo(fraction2) < 0;
        }

        public static bool operator <=(Fraction fraction1, Fraction fraction2)
        {
            return fraction1.CompareTo(fraction2) <= 0;
        }

        public static bool operator ==(Fraction fraction1, Fraction fraction2)
        {
            return fraction1.Equals(fraction2);
        }

        public static bool operator !=(Fraction fraction1, Fraction fraction2)
        {
            return !fraction1.Equals(fraction2);
        }

       
        private void Simplify()
        {
            int gcd = GCD(Math.Abs(numerator), Math.Abs(denominator));
            numerator /= gcd;
            denominator /= gcd;

            if (denominator < 0)
            {
                numerator = -numerator;
                denominator = -denominator;
            }
        }

       
        private int GCD(int a, int b)
        {
            while (b != 0)
            {
                int temp = b;
                b = a % b;
                a = temp;
            }
            return a;
        }

      
        public override string ToString()
        {
            return $"{numerator}/{denominator}";
        }

        
        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
                return false;

            Fraction other = (Fraction)obj;
            return numerator == other.numerator && denominator == other.denominator;
        }

       
        public override int GetHashCode()
        {
            return (numerator.GetHashCode() * 31) ^ (denominator.GetHashCode() * 17);
        }


        public int CompareTo(Fraction other)
        {
            int thisValue = numerator * other.denominator;
            int otherValue = other.numerator * denominator;

            return thisValue.CompareTo(otherValue);
        }
    }

}
